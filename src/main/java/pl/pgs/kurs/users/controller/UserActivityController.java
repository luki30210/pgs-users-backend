package pl.pgs.kurs.users.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.pgs.kurs.users.model.Activity;
import pl.pgs.kurs.users.model.Converters;
import pl.pgs.kurs.users.model.User;
import pl.pgs.kurs.users.model.dto.ActivityDTO;
import pl.pgs.kurs.users.model.service.ActivityService;
import pl.pgs.kurs.users.model.service.UserService;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Łukasz Patro
 * on 16.04.17.
 */
@CrossOrigin
@RestController
@RequestMapping(path = "/users/{login}/activities")
public class UserActivityController {

    @Autowired
    private ActivityService activityService;

    @Autowired
    private UserService userService;

    @PostMapping
    public ResponseEntity<Activity> addActivity(@PathVariable String login, @RequestBody ActivityDTO activityDTO) {

        //Set<User> lista = new HashSet<>();

        try {
            User user = userService.getUser(login);
            Activity activity = Converters.toActivity(activityDTO);
            activity.addUser(user);
            activityService.addActivity(activity);
            return new ResponseEntity<>(activity, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping
    public ResponseEntity<Set<Activity>> getUserActivities(@PathVariable String login) {

        try {
            User user = userService.getUser(login);
            Set<Activity> activities = activityService.getAllUsersActivities(user);
            return new ResponseEntity<>(activities, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<ActivityDTO> getActivity(@PathVariable String login, @PathVariable Long id) {

        try {
            User user = userService.getUser(login);
            Activity activity = activityService.getUsersActivity(user, id);
            return new ResponseEntity<>(Converters.toActivityDTO(activity), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<Activity> putActivity(@PathVariable String login, @RequestBody Activity activity, @PathVariable Long id) {

        try {
            User user = userService.getUser(login);
            activity.setId(id);
            activity.addUser(user);
            activityService.putActivity(activity);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Activity> deleteActivity(@PathVariable String login, @PathVariable Long id) {

        try {
            User user = userService.getUser(login);
            Activity a = activityService.getUsersActivity(user, id);
            activityService.deleteActivity(a.getId());
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

}
