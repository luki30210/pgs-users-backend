package pl.pgs.kurs.users.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.pgs.kurs.users.exception.UserAlreadyExistsException;
import pl.pgs.kurs.users.exception.UserNotFoundException;
import pl.pgs.kurs.users.model.Converters;
import pl.pgs.kurs.users.model.User;
import pl.pgs.kurs.users.model.dto.UserDTO;
import pl.pgs.kurs.users.model.service.UserService;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Łukasz Patro
 * on 08.04.17.
 */
@CrossOrigin
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping
    public ResponseEntity<UserDTO> addUser(@RequestBody UserDTO userDTO) {

        User user = Converters.toUser(userDTO);

        try {
            userService.addUser(user);
            return new ResponseEntity<>(userDTO, HttpStatus.OK);
        } catch (UserAlreadyExistsException e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(userDTO, HttpStatus.CONFLICT);
        }


    }

    @GetMapping(produces = "application/json")
    public ResponseEntity<List<UserDTO>> getUsers() {

        try {
            List<UserDTO> list = userService.getUsersAll().stream().map(Converters::toUserDTO).collect(Collectors.toList());
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping(path = "/{login}")
    public ResponseEntity<UserDTO> getUser(@PathVariable String login) {

        try {
            UserDTO userDTO = Converters.toUserDTO(userService.getUser(login));
            return new ResponseEntity<>(userDTO, HttpStatus.OK);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @PutMapping(path = "/{login}")
    public ResponseEntity<UserDTO> putUser(@RequestBody UserDTO userDTO, @PathVariable String login) {

        userDTO.setLogin(login);
        User user = Converters.toUser(userDTO);
        //user.setLogin(login);
        userService.putUser(user);
        return new ResponseEntity<>(userDTO, HttpStatus.OK);

    }

    @DeleteMapping(path = "/{login}")
    public ResponseEntity<UserDTO> deleteUser(@PathVariable String login) {

        try {
            userService.deleteUser(login);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

}
