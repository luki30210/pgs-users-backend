package pl.pgs.kurs.users.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.pgs.kurs.users.exception.ActivityNotFoundException;
import pl.pgs.kurs.users.model.Activity;
import pl.pgs.kurs.users.model.Converters;
import pl.pgs.kurs.users.model.dto.ActivityDTO;
import pl.pgs.kurs.users.model.service.ActivityService;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Łukasz Patro
 * on 07.05.17.
 */
@RestController
@RequestMapping(path = "/activities")
@CrossOrigin
public class ActivityController {

    @Autowired
    private ActivityService activityService;

    @PostMapping
    public ResponseEntity<ActivityDTO> create(@RequestBody @Valid ActivityDTO activityDTO) {

        Activity activity = Converters.toActivity(activityDTO);
        activityService.addActivity(activity);
        return new ResponseEntity<>(Converters.toActivityDTO(activity), HttpStatus.OK);

    }

    @GetMapping
    public ResponseEntity<List<ActivityDTO>> getAll() {

        try {
            List<ActivityDTO> list = activityService.getAllActivities().stream().map(Converters::toActivityDTO).collect(Collectors.toList());
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (ActivityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<ActivityDTO> getById(@PathVariable Long id) {

        try {
            ActivityDTO activity = Converters.toActivityDTO(activityService.getActivity(id));
            return new ResponseEntity<>(activity, HttpStatus.OK);
        } catch (ActivityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<ActivityDTO> update(@PathVariable(name = "id") Long id, @RequestBody @Valid ActivityDTO activityDTO) {

        activityDTO.setId(id);
        Activity activity = Converters.toActivity(activityDTO);
        activityService.putActivity(activity);
        return new ResponseEntity<>(Converters.toActivityDTO(activity), HttpStatus.OK);

    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<ActivityDTO> delete(@PathVariable(name = "id") Long id) {

        try {
            activityService.deleteActivity(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (ActivityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

}
