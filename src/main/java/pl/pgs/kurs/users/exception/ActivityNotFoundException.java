package pl.pgs.kurs.users.exception;

/**
 * Created by Łukasz Patro
 * on 11.05.17.
 */
public class ActivityNotFoundException extends Exception {

    public ActivityNotFoundException(String reason) {
        super(reason);
    }

}
