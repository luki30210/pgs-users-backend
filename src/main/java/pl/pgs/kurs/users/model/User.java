package pl.pgs.kurs.users.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Łukasz Patro
 * on 08.04.17.
 */
//@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "users")
public class User {

    public enum UserType { LECTURER, STUDENT }

    @Id // primary key
    @NotNull
    //@Column(name = "login")
    private String login;

    @NotNull
    //@Column(name = "first_name")
    private String firstName;

    @NotNull
    //@Column(name = "last_name")
    private String lastName;

    @NotNull
    @JsonFormat(pattern="yyyy-MM-dd")
    //@Column(name = "date_of_birth")
    private Date dateOfBirth;

    private String email;

    @NotNull
    private UserType userType;

    //@JsonManagedReference
    @JsonIgnore
    @ManyToMany(mappedBy = "users")
    private Set<Activity> activities;

    public User() {
        activities = new HashSet<>();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Set<Activity> getActivities() {
        return activities;
    }

    public void setActivities(Set<Activity> activities) {
        this.activities = activities;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }
}
