package pl.pgs.kurs.users.model.repository;

import org.springframework.data.repository.CrudRepository;
import pl.pgs.kurs.users.model.Activity;
import pl.pgs.kurs.users.model.User;
import java.util.Set;

/**
 * Created by Łukasz Patro
 * on 16.04.17.
 */
public interface ActivityRepository extends CrudRepository<Activity, Long> {

    @Override
    Activity findOne(Long id);

    @Override
    Set<Activity> findAll();

    //Set<Activity> findByUser(User user);

    //Activity findByUserAndId(User user, Long id);

    Set<Activity> findByUsers_login(String login);

    Activity findByUsers_loginAndId(String login, Long id);

}
