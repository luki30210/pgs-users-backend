package pl.pgs.kurs.users.model.service;

import pl.pgs.kurs.users.exception.ActivityNotFoundException;
import pl.pgs.kurs.users.model.Activity;
import pl.pgs.kurs.users.model.User;
import java.util.Set;

/**
 * Created by Łukasz Patro
 * on 16.04.17.
 */
public interface ActivityService {

    void addActivity(Activity activity);

    Set<Activity> getAllActivities() throws ActivityNotFoundException;

    Set<Activity> getAllUsersActivities(User user) throws ActivityNotFoundException;

    Activity getActivity(Long id) throws ActivityNotFoundException;

    Activity getUsersActivity(User user, Long id) throws ActivityNotFoundException;

    void putActivity(Activity activity);

    void deleteActivity(Long id) throws ActivityNotFoundException;

    //void updateActivity(Activity oldActivity, Activity newActivity);

}
