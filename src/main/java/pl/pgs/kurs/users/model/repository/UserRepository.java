package pl.pgs.kurs.users.model.repository;

import org.springframework.data.repository.CrudRepository;
import pl.pgs.kurs.users.model.User;
import java.util.List;

/**
 * Created by Łukasz Patro
 * on 09.04.17.
 */
public interface UserRepository extends CrudRepository<User, String> {

    @Override
    User findOne(String login);

    @Override
    List<User> findAll();

    List<User> findByFirstName(String firstName);

    List<User> findByLastName(String lastName);

    List<User> findByUserType(User.UserType userType);

}
