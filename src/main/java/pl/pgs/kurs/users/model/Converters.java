package pl.pgs.kurs.users.model;

import pl.pgs.kurs.users.model.dto.ActivityDTO;
import pl.pgs.kurs.users.model.dto.UserDTO;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Łukasz Patro
 * on 07.05.17.
 */
public class Converters {

    public static User toUser(UserDTO userDTO) {

        User user = new User();

        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setEmail(userDTO.getEmail());
        user.setDateOfBirth(userDTO.getDateOfBirth());
        user.setLogin(userDTO.getLogin());
        user.setUserType(userDTO.getUserType());
        /*try {
            user.setActivities(toActivities(userDTO.getActivities()));
        } catch (Exception e) {
            user.setActivities(new HashSet<Activity>());
        }*/
        user.setActivities(toActivities(userDTO.getActivities()));

        return user;

    }

    public static UserDTO toUserDTO(User user) {

        UserDTO userDTO = new UserDTO();

        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setEmail(user.getEmail());
        userDTO.setDateOfBirth(user.getDateOfBirth());
        userDTO.setLogin(user.getLogin());
        userDTO.setUserType(user.getUserType());
        userDTO.setActivities(toActivitiesDTO(user.getActivities()));

        return userDTO;

    }

    public static Activity toActivity(ActivityDTO activityDTO) {

        Activity activity = new Activity();

        activity.setId(activityDTO.getId());
        activity.setName(activityDTO.getName());
        activity.setStartDate(activityDTO.getStartDate());
        activity.setDescription(activityDTO.getDescription());

        return activity;
    }

    public static Set<Activity> toActivities(Set<ActivityDTO> activitiesDTO) {

        Set<Activity> activities = new HashSet<Activity>();

        if (activitiesDTO == null || activitiesDTO.isEmpty()) {
            return activities;
        }

        for (ActivityDTO activityDTO : activitiesDTO) {
            activities.add(toActivity(activityDTO));
        }

        return activities;
    }

    public static ActivityDTO toActivityDTO(Activity activity) {

        ActivityDTO activityDTO = new ActivityDTO();

        activityDTO.setId(activity.getId());
        activityDTO.setName(activity.getName());
        activityDTO.setStartDate(activity.getStartDate());
        activityDTO.setDescription(activity.getDescription());

        return activityDTO;
    }

    public static Set<ActivityDTO> toActivitiesDTO(Set<Activity> activities) {

        Set<ActivityDTO> activitiesDTO = new HashSet<ActivityDTO>();

        if (activities == null || activities.isEmpty()) {
            return activitiesDTO;
        }

        for (Activity activity : activities) {
            activitiesDTO.add(toActivityDTO(activity));
        }

        return activitiesDTO;
    }

}
