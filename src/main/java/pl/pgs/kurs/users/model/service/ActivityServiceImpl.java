package pl.pgs.kurs.users.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pgs.kurs.users.exception.ActivityNotFoundException;
import pl.pgs.kurs.users.model.Activity;
import pl.pgs.kurs.users.model.User;
import pl.pgs.kurs.users.model.repository.ActivityRepository;

import java.util.Set;

/**
 * Created by Łukasz Patro
 * on 16.04.17.
 */
@Service
public class ActivityServiceImpl implements ActivityService {

    @Autowired
    private ActivityRepository activityRepository;

    @Override
    public void addActivity(Activity activity) {

        activityRepository.save(activity);

    }

    @Override
    public Set<Activity> getAllActivities() throws ActivityNotFoundException {

        Set<Activity> activities = activityRepository.findAll();

        if (activities.isEmpty()) {
            throw new ActivityNotFoundException("No activities found.");
        } else {
            return activities;
        }

    }

    @Override
    public Set<Activity> getAllUsersActivities(User user) throws ActivityNotFoundException {

        Set<Activity> activities = activityRepository.findByUsers_login(user.getLogin());

        if (activities.isEmpty()) {
            throw new ActivityNotFoundException("User " + user.getLogin() + " have no activities.");
        } else {
            return activities;
        }

    }

    @Override
    public Activity getActivity(Long id) throws ActivityNotFoundException {

        Activity activity = activityRepository.findOne(id);

        if (activity == null) {
            throw new ActivityNotFoundException("Activity doesn't exist.");
        } else {
            return activity;
        }

    }

    @Override
    public Activity getUsersActivity(User user, Long id) throws ActivityNotFoundException {

        Activity activity = activityRepository.findByUsers_loginAndId(user.getLogin(), id);

        if (activity == null) {
            throw new ActivityNotFoundException("User " + user.getLogin() + " have no activity with id " + id + ".");
        } else {
            return activity;
        }

    }

    @Override
    public void putActivity(Activity activity) {

        Activity oldActivity = activityRepository.findOne(activity.getId());
        if (oldActivity == null) {
            activityRepository.save(activity);
        } else {
            oldActivity.setStartDate(activity.getStartDate());
            oldActivity.setName(activity.getName());
            oldActivity.setStartDate(activity.getStartDate());
            oldActivity.setUsers(activity.getUsers());
            oldActivity.setDescription(activity.getDescription());
            activityRepository.save(oldActivity);
        }

    }

    @Override
    public void deleteActivity(Long id) throws ActivityNotFoundException {

        Activity oldActivity = activityRepository.findOne(id);

        if (oldActivity == null) {
            throw new ActivityNotFoundException("Activity with id = " + id + " doesn't exist");
        } else {
            activityRepository.delete(oldActivity);
        }

    }

}
