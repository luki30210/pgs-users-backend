package pl.pgs.kurs.users.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pgs.kurs.users.exception.UserAlreadyExistsException;
import pl.pgs.kurs.users.exception.UserNotFoundException;
import pl.pgs.kurs.users.model.User;
import pl.pgs.kurs.users.model.repository.UserRepository;
import java.util.List;

/**
 * Created by Łukasz Patro
 * on 14.04.17.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void addUser(User user) throws UserAlreadyExistsException {

        User userOld = userRepository.findOne(user.getLogin());

        if (userOld == null) {
            userRepository.save(user);
        } else {
            throw new UserAlreadyExistsException("User " + user.getLogin() + " already exists.");
        }

    }

    @Override
    public List<User> getUsersAll() throws UserNotFoundException {

        List<User> list = userRepository.findAll();
        if (list == null || list.isEmpty()) {
            throw new UserNotFoundException("No users found.");
        } else {
            return list;
        }

    }

    @Override
    public User getUser(String login) throws UserNotFoundException {

        User user = userRepository.findOne(login);

        if (user == null) {
            throw new UserNotFoundException("User " + login + " doesn't exist");
        } else {
            return user;
        }

    }

    @Override
    public List<User> getUsersByFirstName(String firstName) throws UserNotFoundException {

        List<User> list = userRepository.findByFirstName(firstName);
        if (list == null || list.isEmpty()) {
            throw new UserNotFoundException("No users with first name " + firstName + ".");
        } else {
            return list;
        }

    }

    @Override
    public List<User> getUsersByLastName(String lastName) throws UserNotFoundException {

        List<User> list = userRepository.findByLastName(lastName);
        if (list == null || list.isEmpty()) {
            throw new UserNotFoundException("No users with last name " + lastName + ".");
        } else {
            return list;
        }

    }

    @Override
    public List<User> getUsersLecturers() throws UserNotFoundException {

        List<User> list = userRepository.findByUserType(User.UserType.LECTURER);
        if (list == null || list.isEmpty()) {
            throw new UserNotFoundException("No lecturers found.");
        } else {
            return list;
        }

    }

    @Override
    public List<User> getUsersStudents() throws UserNotFoundException {

        List<User> list = userRepository.findByUserType(User.UserType.STUDENT);
        if (list == null || list.isEmpty()) {
            throw new UserNotFoundException("No students found.");
        } else {
            return list;
        }

    }

    @Override
    public void putUser(User user) {

        userRepository.save(user);

    }

    @Override
    public void deleteUser(User user) throws UserNotFoundException {

        this.deleteUser(user.getLogin());

    }

    @Override
    public void deleteUser(String login) throws UserNotFoundException {

        this.getUser(login);
        userRepository.delete(login);

    }

}
