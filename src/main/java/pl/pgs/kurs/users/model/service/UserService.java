package pl.pgs.kurs.users.model.service;

import pl.pgs.kurs.users.exception.UserAlreadyExistsException;
import pl.pgs.kurs.users.exception.UserNotFoundException;
import pl.pgs.kurs.users.model.User;
import java.util.List;

/**
 * Created by Łukasz Patro
 * on 14.04.17.
 */
public interface UserService {

    void addUser(User user) throws UserAlreadyExistsException;

    List<User> getUsersAll() throws UserNotFoundException;

    User getUser(String login) throws UserNotFoundException;

    List<User> getUsersByFirstName(String firstName) throws UserNotFoundException;

    List<User> getUsersByLastName(String lastName) throws UserNotFoundException;

    List<User> getUsersLecturers() throws UserNotFoundException;

    List<User> getUsersStudents() throws UserNotFoundException;

    void putUser(User user);

    void deleteUser(User user) throws UserNotFoundException;

    void deleteUser(String login) throws UserNotFoundException;

}
